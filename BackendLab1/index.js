const express = require('express')
const app = express()
const port = 4000

app.get('/', (req, res) => {
  res.send('Hello World!')
})


app.post('/bmi',(req,res) => {

        let weight = parseFloat(req.query.weight) 
        let height = parseFloat(req.query.height) 
        var result = {}
        if(!isNaN(weight) && !isNaN(height)){
            let bmi = weight / (height * height) 

            result = {
                "status" : 200,
                "bmi"    : bmi
            }
        }
        else{
            result = {
                "status" : 400,
                "message" : "Weight or Heighy is not a number"
            }
        }
        res.send(JSON.stringify(result))

})

app.get('/triangle',(req,res) => {

    let height = parseFloat(req.query.height) 
    let base = parseFloat(req.query.base) 
    
    var result = {}
    if(!isNaN(base) && !isNaN(height)){
        let area = 0.5*base*height

        result = {

            "Area"    : area
        }
    }
    else{
        result = {

            "message" : "Base or Height is not a number"
        }
    }
    res.send(JSON.stringify(result))

})

app.post('/score',(req,res) => {

 
    let score = parseInt(req.query.score) 
    let grade
    
    var result = {}
    if(!isNaN(score) && (score>=0 && score<=100)) {

        if (score >= 80)
            grade = 'A'
        else if (score >= 70)
            grade = 'B'
        else if (score >= 60)
            grade = 'C'
        else if (score >= 50)
            grade = 'D'
        else
            grade = 'E'  


        result = {
            "Name"    : req.query.name,
            "Grade"   : grade
        }
    
    }


    else{
        result = {
            "status" : "Error",
            "message" : "Please enter your name and score 0-100  "
        }
    }

    res.send(JSON.stringify(result))

})


app.get('/hello', (req, res) => {
    res.send('Sawasdee '+req.query.name)
  })

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})