import AboutUs from "../components/AboutUS";

function AboutUsPage(){
    return(
        <div>
           <h2> คณะผู้จัดทำ เว็บนี้</h2> 

           <AboutUs name="เมธาพร"
                  address = "บ้านหนองอีเกิ้ง"
                  province = "หาดใหญ่"/>
            <hr />

            <AboutUs name="ดาด้า"
                  address = "บ้านโคโนฮะ"
                  province = "หาดใหญ่"/>
        </div>

    );
}

export default AboutUsPage