import BMIResult from "../components/ฺBMIResult";
import {useState} from "react";

function BMICalPage(){

    const [name,setName] = useState(" ");
    const [bmiResult,setBmiResult] = useState(" ");
    const [translateResult,setTranslateResult] = useState(" ");

    const [height,setHeight] = useState(" ");
    const [weight,setWeight] = useState(" ");

    function calculateBMI(){
        let h = parseFloat(height);
        let w = parseFloat(weight);
        let bmi = w/(h*h);
        setBmiResult(bmi);

        if (bmi > 25){
            setTranslateResult("อวบแย้วน้าา");
        }
        else{
            setTranslateResult("หุ่นดีสุดๆไปเลยฮะ"); 
        }
           
        
    }

    return(
        <div align="left">
            <div align="center">
                ยินดีต้อนรับสู่เว็บคำนวณ BMI
                <hr/>
                    คุณชื่อ : <input type ="text" 
                                   value = {name}
                                   onChange={ (e) => {setName(e.target.value);} }/> <br/>
                    ส่วนสูง : <input type ="text" 
                                   value = {height}
                                   onChange={ (e) => {setHeight(e.target.value);} }/> <br/> 
                    น้ำหนัก : <input type ="text" 
                                   value = {weight}
                                   onChange={ (e) => {setWeight(e.target.value);} }/> <br/>

                <button onClick={()=> {calculateBMI()} }> Calculate </button>
                {  (bmiResult != 0 ) && 
                    <div>
                    <hr/>
                    นี่ผลการคำนวณจ้า
                    
                    <BMIResult 
                        name = {name}
                        bmi = {bmiResult}
                        result = {translateResult}
                    />
                    </div>
                }
            </div>
        </div>
    );
}


export default BMICalPage;