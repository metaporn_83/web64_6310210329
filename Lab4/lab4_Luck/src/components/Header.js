import {Link } from "react-router-dom";
import './Webstyle.css'

function Header(){
    return(
        <div id="HeaderSY">
            &nbsp;&nbsp;  ยินดีต้อนรับสู่เว็บคำนวณ BMI :  &nbsp;&nbsp;
            <Link to = "/">เครื่องคิดเลข</Link> 
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <Link to = "/about">ผู้จัดทำ</Link> 
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <Link to = "/luck" >LuckyNumber</Link>
            <hr />
        </div>
    );
}

export default Header;