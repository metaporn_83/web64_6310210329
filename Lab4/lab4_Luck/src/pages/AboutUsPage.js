import AboutUs from "../components/AboutUS";

function AboutUsPage(){
    return(
        <div id >
           <h2> คณะผู้จัดทำ เว็บนี้</h2> 

           <AboutUs name="เมธาพร สกุลกาญจน์"
                  address = "บ้านเราเอง"
                  province = "หาดใหญ่"/>
            <hr />

            <AboutUs name="ดาด้า"
                  address = "บ้านโคโนฮะ"
                  province = "หาดใหญ่"/>
        </div>

    );
}

export default AboutUsPage