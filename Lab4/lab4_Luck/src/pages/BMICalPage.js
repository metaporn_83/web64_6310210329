import BMIResult from "../components/ฺBMIResult";
import {useState} from "react";

function BMICalPage(){

    const [name,setName] = useState(" ");
    const [bmiResult,setBmiResult] = useState(" ");
    const [translateResult,setTranslateResult] = useState(" ");

    const [height,setHeight] = useState(" ");
    const [weight,setWeight] = useState(" ");

    function calculateBMI(){
        let h = parseFloat(height);
        let w = parseFloat(weight);
        let bmi = w/(h*h);
        setBmiResult(bmi);

        if (bmi > 30)
            setTranslateResult("โรคอ้วนอันตราย");
        else if (bmi >= 25 && bmi <= 29.9) 
            setTranslateResult("โรคอ้วน");
        else if (bmi >= 23 && bmi <= 24.9) 
            setTranslateResult("น้ำหนักเกิน");
        else if (bmi >= 18.5 && bmi <= 22.9) 
            setTranslateResult("สมส่วน"); 
        else  
            setTranslateResult("น้ำหนักต่ำกว่าเกณฑ์");
           
        
    }

    return(
        <div align="left">
            <div align="center">
                ยินดีต้อนรับสู่เว็บคำนวณ BMI
                <hr/>
                    คุณชื่อ : <input type ="text" 
                                   value = {name}
                                   onChange={ (e) => {setName(e.target.value);} }/> <br/>
                    ส่วนสูง : <input type ="text" 
                                   value = {height}
                                   onChange={ (e) => {setHeight(e.target.value);} }/> <br/> 
                    น้ำหนัก : <input type ="text" 
                                   value = {weight}
                                   onChange={ (e) => {setWeight(e.target.value);} }/> <br/>

                <button onClick={()=> {calculateBMI()} }> Calculate </button>
                {  (bmiResult != 0 ) && 
                    <div>
                    <hr/>
                    นี่ผลการคำนวณจ้า
                    
                    <BMIResult 
                        name = {name}
                        bmi = {bmiResult}
                        result = {translateResult}
                    />
                    </div>
                }
            </div>
        </div>
    );
}


export default BMICalPage;