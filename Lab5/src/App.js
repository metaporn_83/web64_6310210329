import logo from './logo.svg';
import './App.css';

import LuckyNumber from './components/LuckyNumber'
import AboutUsPage from './pages/AboutUsPage'
import BMICalPage from './pages/BMICalPage';
import Header from './components/Header'

import { Routes, Route } from "react-router-dom";


function App() {
  return (
    <div className="App">
       <Header /> 
        
        <Routes>
        <Route path = "about" element ={
                  <AboutUsPage /> }/>

        <Route path = "/" element = {
                  <BMICalPage />} />

        <Route path = "luck" element ={
                  <LuckyNumber />} />

       </Routes>    
    </div>
  );
}

export default App;
