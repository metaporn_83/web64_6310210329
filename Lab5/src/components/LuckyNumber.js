import {useState} from "react";
import Button from '@mui/material/Button';

function LuckyNumber(){

const [userNum,setUserNum ]  = useState(" ");
const [luckNum,setLuckNum] = useState("");
const [showResult,setShowResult] = useState();

const [isShow, setIsShow] = useState(true);
const handleClick = () => {setIsShow(!isShow);}

    function RandomNum(){
        let usNum = parseInt(userNum);
        let luckyNum = 69 ;
        setLuckNum(luckyNum);

    if (usNum == luckyNum)
        setShowResult("ถูกแล้วจ้า")   
    else
        setShowResult("ผิด!!")

    }


    return(
        <div align="center">
            <h3> LUCKY  NUMBER </h3>
            <hr />

            กรุณาทายตัวเลขที่ต้องการ ระหว่าง 0-99 : <input type ="text" 
                                   value = {userNum}
                                   onChange={ (e) => {setUserNum(e.target.value);} }/> &nbsp;&nbsp;

            <Button variant="contained" onClick={()=> {RandomNum()} }>
                    ทาย </Button>
            {isShow &&
                <div>
                    <hr/><br />
                    ผลการทาย = {showResult} <br /><br />

               </div>     
            }

                
    </div>
    );
}

export default LuckyNumber ;