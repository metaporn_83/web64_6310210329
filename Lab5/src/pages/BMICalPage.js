import BMIResult from "../components/ฺBMIResult";
import {useState} from "react";

import {Grid,Box,Typography,Paper,Container,Button} from '@mui/material/';



function BMICalPage(){

    const [name,setName] = useState(" ");
    const [bmiResult,setBmiResult] = useState(" ");
    const [translateResult,setTranslateResult] = useState(" ");

    const [height,setHeight] = useState(" ");
    const [weight,setWeight] = useState(" ");

    function calculateBMI(){
        let h = parseFloat(height);
        let w = parseFloat(weight);
        let bmi = w/(h*h);
        setBmiResult(bmi);

        if (bmi > 30)
            setTranslateResult("โรคอ้วนอันตราย");
        else if (bmi >= 25 && bmi <= 29.9) 
            setTranslateResult("โรคอ้วน");
        else if (bmi >= 23 && bmi <= 24.9) 
            setTranslateResult("น้ำหนักเกิน");
        else if (bmi >= 18.5 && bmi <= 22.9) 
            setTranslateResult("สมส่วน"); 
        else  
            setTranslateResult("น้ำหนักต่ำกว่าเกณฑ์");
           
        
    }
    return (
        <Container maxWidth = 'lg'>
        <Grid container spacing={2} sx={ {marginTop : "10px" }}>
                <Grid item xs={12} >
                    <Typography variant="h5">
                        ยินดีต้อนรับสู่เว็บคำนวณ BMI
                    </Typography>
        </Grid>
            <Grid item xs={8}>
                <Box sx={{ textAlign : "left"}}>
                    
                    คุณชื่อ :  <input type ="text" 
                                   value = {name}
                                   onChange={ (e) => {setName(e.target.value);} }/> <br/>
                    ส่วนสูง :  <input type ="text" 
                                   value = {height}
                                   onChange={ (e) => {setHeight(e.target.value);} }/> <br/> 
                    น้ำหนัก :  <input type ="text" 
                                   value = {weight}
                                   onChange={ (e) => {setWeight(e.target.value);} }/> <br/>
                <br/>
                <Button variant="contained" onClick={()=> {calculateBMI()} }>
                    Calculate </Button>
              </Box>  
        </Grid>
        <Grid item xs={4}>
        {  (bmiResult != 0 ) && 
                    <div align = "center">
                   <hr/> นี่ผลการคำนวณจ้า
                    
                    <BMIResult 
                        name = {name}
                        bmi = {bmiResult}
                        result = {translateResult}
                    />
                    </div>
                }
        </Grid>
      </Grid>
      </Container>
  );
}

export default BMICalPage;